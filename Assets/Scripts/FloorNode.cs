﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshCollider))]

public class FloorNode : MonoBehaviour {

    private Vector2 screenPoint;
    private Vector3 offset;
    public GameObject prevNode;
    public GameObject nextNode;
    public TextMesh dist;
    private Vector2 distVal;
    Renderer rend;
    Color goColor;

    int i;
    LineRenderer lr;
    LineRenderer tlr;

    void Start()
    {
        tlr = GetComponent<LineRenderer>();
        i = int.Parse(gameObject.name);
        prevNode = GameObject.Find((i - 1).ToString()); // .find not great, works for now..
        SetNodeTag();
        rend = GetComponent<Renderer>();
        goColor = rend.material.color;
    }

    void Update()
    {
        if (nextNode == null)
        {
            nextNode = GameObject.Find((i + 1).ToString());
        }
        else if (nextNode != null && prevNode != null)
        {
            lr = prevNode.GetComponent<LineRenderer>();
            lr.enabled = true;
            lr.SetPosition(0, prevNode.transform.position);
            lr.SetPosition(1, transform.position);
            CalculateDistance(gameObject.transform.position, prevNode.transform.position);
        }

        if (prevNode != null && lr != null)
        {
            lr.enabled = true;
            lr.SetPosition(0, prevNode.transform.position);
            lr.SetPosition(1, transform.position);
            CalculateDistance(gameObject.transform.position, prevNode.transform.position);
        }


        if (nextNode == null && tlr != null)
        {
            tlr.enabled = false;
        }
    }

    void OnMouseDown()
    {
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));
    }

    void OnMouseDrag()
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z);
        Vector3 curV3Position = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        Vector2 curPosition = curV3Position;

        // Move Selected node freely
        transform.position = new Vector2(curPosition.x, curPosition.y);

        // Move previous node to the with respect to node tag
        if (prevNode != null)
        {
            if (this.gameObject.tag == StaticStrings.HorizontalNode)
                prevNode.transform.position = new Vector2(curPosition.x, prevNode.transform.position.y);
            else if (this.gameObject.tag == StaticStrings.VerticalNode)
                prevNode.transform.position = new Vector2(prevNode.transform.position.x, curPosition.y);
        }

        // Move previous node to the with respect to node tag
        if (nextNode != null)
        {
            if (this.gameObject.tag == StaticStrings.HorizontalNode)
                nextNode.transform.position = new Vector2(nextNode.transform.position.x, curPosition.y);
            else if (this.gameObject.tag == StaticStrings.VerticalNode)
                nextNode.transform.position = new Vector2(curPosition.x, nextNode.transform.position.y);
        }
    }

    void OnMouseEnter()
    {
        rend.material.color = Color.green;
    }

    void OnMouseExit()
    {
        rend.material.color = goColor;
    }

    void CalculateDistance(Vector2 fromPos, Vector2 toPos)
    {
        Vector2 displayTransform = (fromPos + toPos) / 2;
        distVal = fromPos - toPos;
        float metreDist;
        if (this.gameObject.tag == StaticStrings.VerticalNode)
        {
            metreDist = Mathf.Abs(distVal.x);
            dist.transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        else
        {
            metreDist = Mathf.Abs(distVal.y);
            dist.transform.rotation = Quaternion.Euler(0, 0, -90);
        }

        dist.text = ConvertToCMs(metreDist);
        dist.transform.position = displayTransform;
    }

    private string ConvertToCMs(float metreDist)
    {
        //Assume Metres, convert to CM
        float cmDist = metreDist * 100;
        double roundedVal = System.Math.Round(cmDist, 2);
        string cmDistance = roundedVal.ToString() + " cm";
        return cmDistance;
    }

    void SetNodeTag()
    {
        if (prevNode != null)
        {
            lr = prevNode.GetComponent<LineRenderer>();

            if (prevNode.tag == StaticStrings.VerticalNode)
                this.gameObject.tag = StaticStrings.HorizontalNode;
            else
                this.gameObject.tag = StaticStrings.VerticalNode;
        }
        else
        {
            this.gameObject.tag = StaticStrings.VerticalNode;
        }
    }
}
