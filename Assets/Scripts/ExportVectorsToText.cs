﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class ExportVectorsToText : MonoBehaviour {

    public FloorManager floorManager;
    public VerticesManager doorManager;
    public VerticesManager windowManager;

    //Set to hardcoded filename, will overwrite
    string fileName = StaticStrings.RelativeExportPath + "NodeVectors.txt";

    void Start()
    {
        floorManager = floorManager.GetComponent<FloorManager>();
        doorManager = doorManager.GetComponent<VerticesManager>();
        windowManager = windowManager.GetComponent<VerticesManager>();
    }

    void Update()
    {
        if (doorManager.verticesGOs.Count >= doorManager.minVerticesRequired && windowManager.verticesGOs.Count >= windowManager.minVerticesRequired)
        {
            gameObject.GetComponent<Text>().enabled = true;
            if (Input.GetKeyDown(KeyCode.C))
                ExportTextFile();
        }
        else
        {
            gameObject.GetComponent<Text>().enabled = false;
        }
    }

    void ExportTextFile()
    {
        if (File.Exists(fileName))
            Debug.Log("Overwriting: " + fileName);

        var sr = File.CreateText(fileName);
        sr.WriteLine("Floor Nodes:");
        sr.WriteLine(VectorsToString(floorManager.floorObjects));
        sr.WriteLine("");
        sr.WriteLine("Door Nodes:");
        sr.WriteLine(VectorsToString(doorManager.verticesGOs));
        sr.WriteLine("");
        sr.WriteLine("Window Nodes:");
        sr.WriteLine(VectorsToString(windowManager.verticesGOs));
        sr.Close();
        Debug.Log(fileName + " exported.");
    }

    string VectorsToString(List<GameObject> a)
    {
        string retVal = "";
        if (a.Count != 0)
        {
            foreach (GameObject g in a)
                retVal += g.transform.position.ToString() + ", ";
            retVal = retVal.Substring(0, retVal.Length - 2); // remove last ", "
        }
        return retVal;
    }

}
