﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StaticStrings
{
    // Floors
    public static string LineManager = "LineManager";
    public static string HorizontalNode = "HorNode";
    public static string VerticalNode = "VerNode";

    // Doors 

    // Windows

    //Vertice
    public static string HorizontalVerticeNode = "HorizontalVerticeNode";
    public static string VerticalVerticeNode = "VerticalVerticeNode";

    //Mode Transitions
    public static string FloorMode = "Mode: Floor Mode      Left-Click : Create Node      Right-Click: Delete Node      F: Finish Floor";
    public static string WindowMode = "Mode: Window Mode      Left-Click : Create Node      Right-Click: Delete Node";
    public static string DoorMode = "Mode: Door Mode      Left-Click : Create Node      Right-Click: Delete Node";

    //Relative Export Path
    public static string RelativeExportPath = "Assets/TextExports/";

}