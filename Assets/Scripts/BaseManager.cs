﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseManager : MonoBehaviour {

    protected virtual void Start()
    {

    }

    public GameObject isNodeClick(GameObject selectedGameObject, string horNode, string verNode)
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit) && ((hit.transform.tag == horNode) || (hit.transform.tag == verNode)))
        {
            selectedGameObject = GameObject.Find(hit.transform.name);
            return selectedGameObject;
        }
        return null;
    }

    public GameObject InstantiateNode(GameObject Node, Vector2 screenPoint, List<GameObject> GameObjects, Transform parent)
    {
        GameObject instantiatedNode = Instantiate(Node, screenPoint, Quaternion.identity);
        GameObjects.Add(instantiatedNode);
        instantiatedNode.transform.parent = parent;
        instantiatedNode.name = GameObjects.Count.ToString();
        Debug.Log("Node Added");
        return instantiatedNode;
    }

    public bool DeleteLatestNode(List<GameObject> GameObjects, string stringArea)
    {
        GameObject goToDelete = GameObject.Find(stringArea + "/" + GameObjects.Count.ToString());
        if (goToDelete)
        {
            GameObjects.Remove(GameObjects.Last()); // may now always want to remove last
            Destroy(goToDelete);
            return true;
        }
        else
        {
            Debug.Log("No More Nodes to Destroy");
            return false;
        }
    }

    public void UpdateManagerStatus(bool isSectionComplete, Text isSectionCompleteText)
    {
        if (isSectionComplete)
            isSectionCompleteText.color = Color.green;
        else
            isSectionCompleteText.color = Color.red;
    }

    void CalculateDistance(Vector2 fromPos, Vector2 toPos, Vector2 distVal, TextMesh dist)
    {
        Vector2 displayTransform = (fromPos + toPos) / 2;
        distVal = fromPos - toPos;
        float metreDist;
        if (this.gameObject.tag == StaticStrings.VerticalNode)
        {
            metreDist = Mathf.Abs(distVal.x);
            dist.transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        else
        {
            metreDist = Mathf.Abs(distVal.y);
            dist.transform.rotation = Quaternion.Euler(0, 0, -90);
        }

        dist.text = ConvertToCMs(metreDist);
        dist.transform.position = displayTransform;
    }

    private string ConvertToCMs(float metreDist)
    {
        //Assume Metres, convert to CM
        float cmDist = metreDist * 100;
        double roundedVal = System.Math.Round(cmDist, 2);
        string cmDistance = roundedVal.ToString() + " cm";
        return cmDistance;
    }
}
