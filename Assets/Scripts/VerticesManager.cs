﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VerticesManager : BaseManager
{
    public BaseManager bm;
    public GameObject Node;
    public List<GameObject> verticesGOs;
    public GameObject VerticeArea;
    public GameObject floorMesh;
    public GameObject selectedGameObject;
    public string nodeTag;
    public bool canResize;

    private Vector2 screenPoint;
    private Vector2 max;
    private Vector2 min;

    public int minVerticesRequired;
    public Text isSectionComplete;

    protected override void Start()
    {
        base.Start();    // call base class
        bm.UpdateManagerStatus(MinVerticesCheck(), isSectionComplete);
    }

    private bool MinVerticesCheck()
    {
        if (verticesGOs.Count >= minVerticesRequired)
            return true;
        return false;
    }

    void Update()
    {
        screenPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (verticesGOs == null && Input.GetMouseButtonDown(0))
            verticesGOs = new List<GameObject>();

        if (verticesGOs != null)
            bm.UpdateManagerStatus(MinVerticesCheck(), isSectionComplete);

        if (Input.GetMouseButtonDown(0))
            selectedGameObject = bm.isNodeClick(selectedGameObject, StaticStrings.HorizontalVerticeNode, StaticStrings.VerticalVerticeNode);

        if (verticesGOs != null && Input.GetMouseButtonDown(0) && selectedGameObject == null)
        {
            Vector2 closestMidPoint = FindClosestMidPoint(screenPoint);
            GameObject instantiatedNode = bm.InstantiateNode(Node, closestMidPoint, verticesGOs, VerticeArea.transform);
            instantiatedNode.tag = nodeTag;
            if (nodeTag.Contains("Vertical"))
                instantiatedNode.transform.rotation = Quaternion.Euler(0, 0, -90); //messy but works for now..
            SetNodeBoundaries(instantiatedNode);
        }

        if (Input.GetMouseButtonDown(1) && bm.DeleteLatestNode(verticesGOs, VerticeArea.name))
            Debug.Log("Node Deleted");
    }

    void SetCorrectNodeTag(Vector2 fromPosition, Vector2 toPosition)
    {
        float x = (fromPosition.x - toPosition.x);
        float y = (fromPosition.y - toPosition.y);
        if (Mathf.Abs(x) < Mathf.Abs(y))
            nodeTag = StaticStrings.VerticalVerticeNode;
        else
            nodeTag = StaticStrings.HorizontalVerticeNode;
    }

    Vector2 FindClosestMidPoint(Vector2 screenPoint)
    {
        LineRenderer alr = floorMesh.GetComponent<LineRenderer>();
        float closestVal = 0;
        Vector2 retVal = new Vector2();
        Vector2 midPoint = new Vector2();
        int next = 0;

        for (int i = 0; i < alr.positionCount; i++)
        {
            if (i == alr.positionCount - 1)
                next = 0;
            else
                next = i + 1;

            //Get the midPoint between the two vectors
            midPoint = (alr.GetPosition(i) + alr.GetPosition(next)) / 2;
            float tempDist = Vector2.Distance(screenPoint, midPoint);

            // Find closest and set the first to always
            if (tempDist < closestVal || i == 0)
            {
                closestVal = tempDist;
                retVal = midPoint;
                max = alr.GetPosition(i);
                min = alr.GetPosition(next);
            }
        }
        SetCorrectNodeTag(max, min);
        return retVal;
    }

    private void SetNodeBoundaries(GameObject instantiatedNode)
    {
        VerticesNode wn = instantiatedNode.GetComponent<VerticesNode>();
        wn.max = max;
        wn.min = min;
    }

}
