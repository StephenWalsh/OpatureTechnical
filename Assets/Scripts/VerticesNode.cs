﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshCollider))]

public class VerticesNode : MonoBehaviour {

    private Vector2 screenPoint;
    private Vector3 offset;
    Renderer rend;
    Color goColor;
    public bool isValid;
    public bool canResize;
    public TextMesh dist;
    private float distVal;
    private float distValx;
    private float distValy;

    // Vertices Node vector limits
    public Vector2 max;
    public Vector2 min;
    float max_x;
    float max_y;
    float min_x;
    float min_y;

    Vector2 initPosition;
    Vector3 initScale;
    Vector3 initTextScale;

    void Start()
    {
        initScale = transform.localScale;
        initTextScale = dist.transform.localScale;
        initPosition = transform.position;
        rend = GetComponent<Renderer>();
        goColor = rend.material.color;
        max_x = Mathf.Max(min.x, max.x);
        max_y = Mathf.Max(min.y, max.y);
        min_x = Mathf.Min(min.x, max.x);
        min_y = Mathf.Min(min.y, max.y);
        Init();
    }

    void Init()
    {
        if (this.gameObject.tag == StaticStrings.VerticalVerticeNode)
        {
            distVal = (rend.bounds.size.y) * transform.localScale.y;
            dist.transform.rotation = Quaternion.Euler(0, 0, 90);
        }
        else
        {
            distVal = (rend.bounds.size.x) * transform.localScale.y;
            dist.transform.rotation = Quaternion.Euler(0, 0, 0);
        }
    }

    void Update()
    {
        IsVerticePositionValid();
        dist.text = ConvertToCMs(distVal).ToString();
    }

    void IsVerticePositionValid()
    {
        //Take into consideration the GO size against the node boundaries, otherwise will only check GO transform pos
        float xVerticeMax = transform.position.x + distVal;
        float xVerticeMin = transform.position.x - distVal;
        float yVerticeMax = transform.position.y + distVal;
        float yVerticeMin = transform.position.y - distVal;

        if ((xVerticeMax < max_x && xVerticeMin > min_x) || (yVerticeMax < max_y && yVerticeMin > min_y))
        {
            isValid = true;
            rend.material.color = goColor;
        }
        else
        {
            isValid = false;
            rend.material.color = Color.red;
        }
    }

    void OnMouseDown()
    {
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));
    }

    void OnMouseUp()
    {
        if (!isValid)
        {
            transform.localScale = initScale;
            dist.transform.localScale = initTextScale;
            transform.position = initPosition;
            Init();
        }
    }

    void OnMouseDrag()
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z);
        Vector3 curV3Position = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        Vector2 curPosition = curV3Position;

        // Move next node to the appropriate direction if available
        if (this.gameObject.tag == StaticStrings.VerticalVerticeNode)
        {
            transform.position = new Vector2(transform.position.x, curPosition.y);
            NodeSizeHandler(rend.bounds.size.x, curPosition.x);
        }
        else if (this.gameObject.tag == StaticStrings.HorizontalVerticeNode)
        {
            transform.position = new Vector2(curPosition.x, transform.position.y);
            NodeSizeHandler(rend.bounds.size.y, curPosition.y);
        }
    }

    void NodeSizeHandler(float rendBounds, float localeScaleX)
    {
        distVal = rendBounds;
        if (canResize)
        {
            transform.localScale = new Vector3(Mathf.Abs(localeScaleX), .5f, 1f);
            distVal = distVal * transform.localScale.x;
            dist.transform.localScale = new Vector3(Mathf.Abs(1 / localeScaleX) *.5f, 1f, 1f);
        }
    }

    private string ConvertToCMs(float metreDist)
    {
        //Assume Metres, convert to CM
        float cmDist = metreDist * 100;
        double roundedVal = System.Math.Round(cmDist, 2);
        string cmDistance = roundedVal.ToString() + " cm";
        return cmDistance;
    }
}
