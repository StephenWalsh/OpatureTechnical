﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModeTransition : MonoBehaviour {

    public GameObject FloorManager;
    public GameObject WindowManager;
    public GameObject DoorManager;
    public Text mode;

    public GameObject floorMesh;
    public FloorManager fm;
    private bool canChangeToFloorMode;

    void Start()
    {
        mode.text = StaticStrings.FloorMode;
        FloorManager.SetActive(true);
        WindowManager.SetActive(false);
        DoorManager.SetActive(false);
        fm = fm.GetComponent<FloorManager>();
    }

    void Update ()
    {
        if (WindowManager.GetComponent<VerticesManager>().verticesGOs.Count != 0 || DoorManager.GetComponent<VerticesManager>().verticesGOs.Count != 0)
            canChangeToFloorMode = false;
        else
            canChangeToFloorMode = true;

        TransitionModes();
    }

    void TransitionModes()
    {
        if (Input.GetKeyDown("1") && canChangeToFloorMode)
        {
            LineRenderer alr = floorMesh.GetComponent<LineRenderer>();
            alr.positionCount = 0;
            DisplayNodes(true);

            mode.text = StaticStrings.FloorMode;
            FloorManager.SetActive(true);
            WindowManager.SetActive(false);
            DoorManager.SetActive(false);
        }
        if (Input.GetKeyDown("2") && fm.isFloorComplete )
        {
            DrawOutline();
            DisplayNodes(false);
            mode.text = StaticStrings.DoorMode;
            FloorManager.SetActive(false);
            WindowManager.SetActive(false);
            DoorManager.SetActive(true);
        }
        if (Input.GetKeyDown("3") && fm.isFloorComplete)
        {
            DrawOutline();
            DisplayNodes(false);
            mode.text = StaticStrings.WindowMode;
            FloorManager.SetActive(false);
            WindowManager.SetActive(true);
            DoorManager.SetActive(false);
        }
    }

    private void DrawOutline()
    {
        LineRenderer alr = floorMesh.GetComponent<LineRenderer>();
        alr.positionCount = fm.floorObjects.Count;
        for (int i = 0; i < fm.floorObjects.Count; i++)
        {
            alr.SetPosition(i, fm.floorObjects[i].transform.position);
        }

        alr.positionCount = fm.floorObjects.Count + 1;
        alr.SetPosition(fm.floorObjects.Count, fm.floorObjects.First().transform.position);
    }

    private void DisplayNodes(bool isDisplay)
    {
        for (int i = 0; i < fm.floorObjects.Count; i++)
        {
            fm.floorObjects[i].SetActive(isDisplay);
        }
        if (isDisplay)
        {
            LineRenderer lr = floorMesh.GetComponent<LineRenderer>();
            lr.positionCount = 0;
        }

    }

}
