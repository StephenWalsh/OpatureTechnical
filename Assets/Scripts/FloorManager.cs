﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloorManager : BaseManager
{
    public BaseManager bm;
    public GameObject Node;
    public GameObject selectedGameObject;
    public GameObject floorMesh;
    public Text isSectionCompleteText;
    public Text SurfaceArea;
    public List<GameObject> floorObjects;
    private Vector2 screenPoint;
    public bool isFloorComplete = false;


    protected override void Start()
    {
        base.Start();    // call base class
        SurfaceArea.text = "Surface Area: 0m²";//default
    }

    void CheckNullReferences()
    {
        if (bm == null)
            Debug.LogError("Missing bm BaseManager class");
        if (Node == null)
            Debug.LogError("Missing Node GameObject");
        if (floorMesh == null)
            Debug.LogError("Missing floorMesh GameObject");
        if (isSectionCompleteText == null)
            Debug.LogError("Missing isSectionCompleteText UI Text");
    }

    void Update()
    {
        CheckNullReferences();
        bm.UpdateManagerStatus(isFloorComplete, isSectionCompleteText);
        screenPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (floorObjects == null && Input.GetMouseButtonDown(0))
            floorObjects = new List<GameObject>();

        if (Input.GetMouseButtonDown(0) && !isFloorComplete)
            FloorNodeInstantiate();

        if (isFloorComplete)
            DrawFloor(Get2DVectorArray());

        if (Input.GetMouseButtonDown(1) && floorObjects != null && !isFloorComplete)
            if (bm.DeleteLatestNode(floorObjects, floorMesh.name))
                Debug.Log("Node removed successfully");

        if (Input.GetKeyDown(KeyCode.F) && floorObjects.Last().tag == StaticStrings.HorizontalNode)
            CompleteFloor();

    }

    private void FloorNodeInstantiate()
    {
        selectedGameObject = bm.isNodeClick(selectedGameObject, StaticStrings.HorizontalNode, StaticStrings.VerticalNode);

        if (floorObjects.Count == 0 && !selectedGameObject)
            bm.InstantiateNode(Node, screenPoint, floorObjects, floorMesh.transform);
        else if (!selectedGameObject)
        {
            Vector2 fromPosition = floorObjects.Last().transform.position;
            Vector2 toPosition = screenPoint;
            screenPoint = PerpendicularNodePosition(fromPosition, toPosition);
            if (CheckInstantiatedNodeDist(fromPosition, toPosition))
                bm.InstantiateNode(Node, screenPoint, floorObjects, floorMesh.transform);
        }
    }

    private void CompleteFloor()
    {
        //add first to last and last to first
        FloorNode t1 = floorObjects.Last().GetComponent<FloorNode>();
        FloorNode t2 = floorObjects.First().GetComponent<FloorNode>();
        Vector2[] vertices2D;

        if (!isFloorComplete)
        {
            isFloorComplete = true;
            t1.nextNode = floorObjects.First();
            t2.prevNode = floorObjects.Last();
            floorObjects.Last().transform.position = new Vector2(floorObjects.Last().transform.position.x, t1.nextNode.transform.position.y);
            vertices2D = Get2DVectorArray();
        }
        else
        {
            isFloorComplete = false;
            t1.nextNode = null;
            t2.prevNode = null;
            vertices2D = new Vector2[] { new Vector2(0, 0) };
        }
        DrawFloor(vertices2D);
    }

    private bool CheckInstantiatedNodeDist(Vector2 fromPosition, Vector2 toPosition)
    {
        float x = Mathf.Abs(fromPosition.x - toPosition.x);
        float y = Mathf.Abs(fromPosition.y - toPosition.y);

        if (x < .2f || y < .2f)
            return false;
        return true;
    }

    private Vector2 PerpendicularNodePosition(Vector2 fromPosition, Vector2 toPosition)
    {
        if (floorObjects.Last())
        {
            if (floorObjects.Last().tag == StaticStrings.VerticalNode)
                toPosition.x = fromPosition.x;
            else
                toPosition.y = fromPosition.y;
        }

        return toPosition;
    }

    private Vector2[] Get2DVectorArray()
    {
        List<Vector2> vertices2DList = new List<Vector2>();
        foreach (GameObject g in floorObjects)
        {
            vertices2DList.Add(g.transform.position);
        }
        return vertices2DList.ToArray();
    }

    private void DrawFloor(Vector2[] floorPointVertices)
    {
        // Create Vector2 vertices
        Vector2[] vertices2D = floorPointVertices.ToArray();

        // Use the triangulator to get indices for creating triangles
        Triangulator tr = new Triangulator(vertices2D);
        int[] indices = tr.Triangulate();

        // Create the Vector3 vertices
        Vector3[] vertices = new Vector3[vertices2D.Length];
        for (int i = 0; i < vertices.Length; i++)
        {
            vertices[i] = new Vector3(vertices2D[i].x, vertices2D[i].y, 0);
        }

        // Create the mesh
        Mesh msh = new Mesh();
        msh.vertices = vertices;
        msh.triangles = indices;
        msh.RecalculateNormals();
        msh.RecalculateBounds();

        // Set up game object with mesh;
        floorMesh.GetComponent(typeof(MeshRenderer));
        MeshFilter filter = floorMesh.GetComponent(typeof(MeshFilter)) as MeshFilter;
        filter.mesh = msh;

        //Set Surface Area this.gameObject.transform.GetChild(0)
        SurfaceArea.text = "Surface Area: " + Area(floorPointVertices).ToString() + "m²";
    }

    public float Area(Vector2[] mVertices)
    {
        Vector3 result = Vector3.zero;
        for (int p = mVertices.Length - 1, q = 0; q < mVertices.Length; p = q++)
        {
            result += Vector3.Cross(mVertices[q], mVertices[p]);
        }
        result *= 0.5f;
        return result.magnitude;
    }
}